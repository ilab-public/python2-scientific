# Changelog

## python2-scientific  2018-01-15 (released Jan 15, 2018)
[FIX] : fixed segmentation fault (core dumped) issue in eccodes gitb_ls
[ENH] : eccodes : upgraded to version 2.6.0
[ENH] : jasper : upgraded to version 2.0.14