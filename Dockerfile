# Use the official latest python 2.7 image (currently 2.7.16-buster)
FROM python:2.7

LABEL maintainer=dirk.devriendt@3e.eu

COPY odbcinst.ini /etc/
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y --no-install-recommends \
    apt-utils \
    build-essential \
    cifs-utils \
    cmake \
    curlftpfs \
    freetds-dev \
    ftp \
    gcc-multilib \
    gfortran \
    git-core \
    ibus \
    libatlas-base-dev \
    libgeos-dev \
    liblapack-dev \
    libudunits2-dev \
    nano \
    nfs-common \
    pandoc \
    r-base \
    samba \
    tdsodbc \
    udunits-bin \
    unixodbc \
    unixodbc-dev \
    unzip \
    vim-tiny \
 && rm -rf /var/lib/apt/lists/*


# install R zoo package
RUN Rscript -e "install.packages('zoo', repos='http://cran.us.r-project.org/')"

ENV MPLBACKEND=agg

COPY requirements.txt .

# steps below will always be executed if `CACHE_DATE` is changed to a new value
ARG CACHE_DATE=2019-07-26
RUN pip install --no-cache-dir --upgrade pip \
 && pip install --no-cache-dir --upgrade Cython numexpr numpy scipy pandas matplotlib \
 && pip install --no-cache-dir --upgrade -r requirements.txt

CMD ["/bin/bash"]
ENTRYPOINT []
